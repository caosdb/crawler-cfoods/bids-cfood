from caosdb.utils.plantuml import to_graphics
from caosadvancedtools.models.parser import parse_model_from_yaml

model = parse_model_from_yaml('src/bids-cfood/model.yml')
to_graphics(model.values(), "model")
