
# README

## Welcome

This is a module for the [LinkAhead-Crawler](https://gitlab.com/linkahead/linkahead-crawler/)
that can read folder structures that comply with the 
[BIDS specification](https://bids-specification.readthedocs.io).

## Setup

Please read the [README_SETUP.md](README_SETUP.md) for instructions on how to
setup this code.

## Design
The BIDS Standard defines rules for how data is stored in files and how those
(and directories) must be named. Those rules can be checked with the
`bids-validator`. The current implementation of this cfood and the corresponding
[converter](https://gitlab.indiscale.com/linkahead/src/crawler-converters/bids-converter)
does the following:
- the `dataset_description.json` is parsed and translated to LinkAhead Entities
- the key value pairs of folder and file names (e.g. "sub-<sub-label>") are
  parsed and the stored in appropriate Records
- the contents of measurement folders such as 'eeg', 'func' or 'anat' are
  assigned to DataAcquisition Records. Each Record shall reference the full
  information for the acquisition. Thus files are typically grouped according
  to their values of the 'acq', 'run', and possibly 'task' keys. If files
  correspond to multiple acquisitions (the filename is missing some key value
  pairs), then those files are referenced by all corresponding Records. This
  grouping is done by the BIDS<datatype>DirConverter classes of the
  bids-converter.
- Based on the suffix of files parents for files are selected.
- The data model references the corresponding BIDS documentation in the
  description where appropriate.
- The session folder may be omitted in BIDS. The crawler always creates at least
  one session. See BIDSSubjectDirConverter classes of the bids-converter.


## Notes
- Currently the folders `sourcedata` and `derivates` are being ignored.
- Currently parametric maps are ignored (ANAT)

## Further Reading

Please refer to the [official documentation](https://docs.indiscale.com/caosdb-crawler/) of the LinkAhead Crawler for more information.

## Contributing

Thank you very much to all contributers—[past,
present](https://gitlab.com/linkahead/linkahead/-/blob/main/HUMANS.md), and prospective
ones.

### Code of Conduct

By participating, you are expected to uphold our [Code of
Conduct](https://gitlab.com/linkahead/linkahead/-/blob/main/CODE_OF_CONDUCT.md).

### How to Contribute

* You found a bug, have a question, or want to request a feature? Please 
[create an issue](https://gitlab.com/linkahead/crawler-extensions/bids-cfood/-/issues).
* You want to contribute code?
    * **Forking:** Please fork the repository and create a merge request in GitLab and choose this repository as
      target. Make sure to select "Allow commits from members who can merge the target branch" under
      Contribution when creating the merge request. This allows our team to work with you on your
      request.
    * **Code style:** This project adhers to the PEP8 recommendations, you can test your code style
      using the `autopep8` tool (`autopep8 -i -r ./`).  Please write your doc strings following the
      [NumpyDoc](https://numpydoc.readthedocs.io/en/latest/format.html) conventions.
* You can also  join the LinkAhead community on
  [#linkahead:matrix.org](https://matrix.to/#/!unwwlTfOznjEnMMXxf:matrix.org).

## License

* Copyright (C) 2022 Research Group Biomedical Physics, Max Planck Institute
  for Dynamics and Self-Organization Göttingen.
* Copyright (C) 2022 Indiscale GmbH <info@indiscale.com>

All files in this repository are licensed under a [GNU Affero General Public
License](LICENCE.md) (version 3 or later).
