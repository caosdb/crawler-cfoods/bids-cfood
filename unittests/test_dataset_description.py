#!/usr/bin/env python3
# encoding: utf-8
#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2022 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2022 Henrik tom Wörden <h.tomwoerden@indiscale.com>
# Copyright (C) 2022 Florian Spreckelsen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

"""
module description
"""
import json
import os

import caosdb as db


from caoscrawler.crawl import Crawler
from caoscrawler.converters import JSONFileConverter, DictConverter
from caoscrawler.identifiable_adapters import CaosDBIdentifiableAdapter
from caoscrawler.structure_elements import File, JSONFile, Directory


def rfp(*pathcomponents):
    """
    Return full path.
    Shorthand convenience function.
    """
    return os.path.join(os.path.dirname(__file__), *pathcomponents)


def test_dataset():
    crawler_definition_path = rfp("../src/bids-cfood/cfood.yml")

    ident = CaosDBIdentifiableAdapter()
    ident.load_from_yaml_definition(rfp("../src/bids-cfood/identifiables.yml"))

    crawler = Crawler(debug=True, identifiableAdapter=ident)
    crawler_definition = crawler.load_definition(crawler_definition_path)
    # print(json.dumps(crawler_definition, indent=3))
    # Load and register converter packages:
    converter_registry = crawler.load_converters(crawler_definition)
    # print("DictIntegerElement" in converter_registry)

    records = crawler.start_crawling(
        Directory('json_only', rfp("test_data/json_only")),
        crawler_definition,
        converter_registry,
        restricted_path=["json_only", 'dataset_description.json']
    )
    subd = crawler.debug_tree
    subc = crawler.debug_metadata
    # print(json.dumps(subc, indent=3))
    # print(subd)
    # print(subc)
    print(records)

    dataset_candiates = [el for el in records if el.parents[0].name == "Dataset"]
    assert len(dataset_candiates) == 1
    dataset = dataset_candiates[0]
    assert dataset.name == "The mother of all experiments"
    assert dataset.get_property("BIDSVersion").value == "1.6.0"
    assert dataset.get_property("HEDVersion").value == "8.0.0"
    assert dataset.get_property("License").value == "CC0"
    assert dataset.get_property("DatasetType").value == "raw"
    assert dataset.get_property("Acknowledgements").value.startswith(
        "Special thanks to Korbinian Brodmann for help in formatting this dataset in")
    assert dataset.get_property(
        "HowToAcknowledge").value == "Please cite this paper: https://www.ncbi.nlm.nih.gov/pubmed/001012092119281"
    assert dataset.get_property("DatasetDOI").value == "doi:10.0.2.3/dfjj.10"
    assert len(dataset.get_property("Author").value) == 2
    persons = [el for el in dataset.get_property("Author").value if isinstance(el, db.Entity)]
    assert len(persons) == 2
    assert "Paul Broca" in [el.get_property("lastName").value for el in persons]
    assert "Carl" in [el.get_property("firstname").value for el in persons if
                      el.get_property("firstname") is not None]
    funding = [el for el in dataset.get_property("Funding").value if isinstance(el, db.Entity)]
    assert "National Institute of Neuroscience Grant F378236MFH1" in [el.name for el in funding]
    print(dataset.get_property("EthicsApproval").value)
    assert (dataset.get_property("EthicsApproval").value[0].startswith(
        "Army Human Research Protections"))
    assert len([el for el in dataset.get_property("ReferencesAndLinks").value if el.startswith(
        "https://www.ncbi.")]) == 1
    assert len([el for el in dataset.get_property("ReferencesAndLinks").value if el.startswith(
        "Alzheimer A., & Kraepelin,")]) == 1
    generatedby = dataset.get_property("Provenance").value
    assert len(generatedby) == 1
    assert generatedby[0].name == "reproin"
    assert generatedby[0].description == "a simple pipeline"
    assert generatedby[0].get_property("Version").value == "0.6.0"
    assert generatedby[0].get_property("CodeURL").value == "https://gitlab.indiscale.com"
    cont = dataset.get_property("Container").value
    assert cont.get_property("Type").value == "docker"
    assert cont.get_property("Tag").value == "repronim/reproin:0.6.0"
    assert cont.get_property("URI").value == "https://indiscale.com"
    source = dataset.get_property("SourceDataset").value[0]
    assert source.get_property("URL").value == "s3://dicoms/studies/correlates"
    assert source.get_property("Version").value == "April 11 2011"
    assert source.get_property("DOI").value == "doi:10.0.2.3/dfjj.11"


if __name__ == "__main__":
    test_dataset()
