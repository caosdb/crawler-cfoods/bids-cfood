# BIDS-CFood

## Requirements

Requires `bidsconverter` module:
https://gitlab.indiscale.com/caosdb/src/crawler-converters/bids-converter Also
`caosdb`,`caosadvancedtools` and `caosdb-crawler` of course. Also see the
[reuirements.txt](./requirements.txt) for more information about the required
packages.

You can install the requirements using pip with

```sh
pip3 install --user -r requirements.txt
```

## Testing

### Unit Tests
Run `pytest unittests`

### Integration Tests
After you started a server that mounts the test data (possibly use `test-profile` in `integrationtests` folder)
you can run `pytest integrationtests`.

### Manual Tests
You can crawl examples from the `bids-examples` submodule. The follwing shows it for the `eeg_matchingpennies` data set:
```sh
# all commands are assumed to be run from the bids-cfood folder
# add the model
python -m caosadvancedtools.models.parser src/bids-cfood/model.yml --sync
# insert files
python -m caosadvancedtools.loadFiles  -p /examples /opt/caosdb/mnt/extroot/examples/eeg_matchingpennies
# run crawler
caosdb-crawler --debug -s update -i src/bids-cfood/identifiables.yml --remove-prefix /bids-examples --add-prefix /examples src/bids-cfood/cfood.yml   bids-examples/eeg_matchingpennies
```

## Usage
You will need to copy (parts of) the cfood definition `./src/bids-cfood/cfood.yml`
into your cfood.




