#!/usr/bin/env python3
# encoding: utf-8
#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2022 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2022 Henrik tom Wörden <h.tomwoerden@indiscale.com>
# Copyright (C) 2022 Florian Spreckelsen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

import sys
import json
import logging
import pytest
import os

import caosdb as db


from caoscrawler.crawl import Crawler
from caoscrawler.converters import JSONFileConverter, DictConverter
from caoscrawler.identifiable_adapters import CaosDBIdentifiableAdapter
from caoscrawler.structure_elements import File, JSONFile, Directory
from caosadvancedtools.models.parser import parse_model_from_yaml

from caosdb.utils.register_tests import clear_database, set_test_key
set_test_key("10b128cf8a1372f30aa3697466bb55e76974e0c16a599bb44ace88f19c8f61e2")


def rfp(*pathcomponents):
    """
    Return full path.
    Shorthand convenience function.
    """
    return os.path.join(os.path.dirname(__file__), *pathcomponents)


@pytest.fixture
def usemodel():
    model = parse_model_from_yaml(rfp("../src/bids-cfood/model.yml"))
    model.sync_data_model(noquestion=True, verbose=False)


def test_folders(clear_database, usemodel):
    crawler_definition_path = rfp("../src/bids-cfood/cfood.yml")

    ident = CaosDBIdentifiableAdapter()

    ident.load_from_yaml_definition(rfp("../src/bids-cfood/identifiables.yml"))

    crawler = Crawler(debug=True, identifiableAdapter=ident)
    crawler_definition = crawler.load_definition(crawler_definition_path)

    # Load and register converter packages:
    converter_registry = crawler.load_converters(crawler_definition)

    logger = logging.getLogger("caoscrawler")
    logger.setLevel(logging.DEBUG)
    records = crawler.start_crawling(
        Directory('valid_dataset', rfp("../unittests/test_data/valid_dataset")),
        crawler_definition,
        converter_registry
    )
    crawler.save_debug_data("prov.yml")
    files = [fi for fi in records if isinstance(fi, db.File)]
    for fi in files:
        fi.file = __file__
    datasets = [el for el in records if el.parents[0].name == "Dataset"]
    subjects = [el for el in records if el.parents[0].name == "Subject"]
    sessions = [el for el in records if el.parents[0].name == "Session"]
    assert len(datasets) == 1
    assert len(subjects) == 5
    assert len(sessions) == 10
    ins, ups = crawler.synchronize(commit_changes=False)
    funcs = [el for el in ins + ups if el.parents[0].name == "FunctionalImaging"]
    anats = [el for el in ins + ups if el.parents[0].name == "AnatomyImaging"]
    assert len(funcs) == 30
    assert len(anats) == 10
    for el in funcs:
        assert (len(el.get_property("FunctionalImagingDataFile").value) == 2
                or len(el.get_property("FunctionalImagingDataFile").value) == 3)
